import requests

class YoutubeVideoList:

    def __init__(self,api_key):
        self._api_key = api_key

    def get_videolist(self,username):
        uploads = self._get_uploads( username )

        video_list = []
        next_page_token = None
        while True:
            items, next_page_token = self._get_playlistitems( uploads, next_page_token=next_page_token )
            video_list += items
            if not next_page_token:
                break

        video_list = self._inflate_video_list( video_list )

        return video_list

    def _get_uploads(self,username):
        r = self._channels_request( part='contentDetails', forUsername=username )
        response = r.json()
        items = response['items']
        if len(items) != 1 :
            raise Exception('Items count != 1')
        item = items[0]
        uploads = item['contentDetails']['relatedPlaylists']['uploads']
        return uploads

    def _get_playlistitems(self,playlist,next_page_token=None):
        args = { 'part': 'snippet', 'playlistId': playlist }
        if next_page_token:
            args['pageToken'] = next_page_token
        r = self._playlistitems_request( **args )
        response = r.json()
        items = response['items']
        items = list(
            map(
                lambda item: {
                    'title': item['snippet']['title'],
                    'videoId': item['snippet']['resourceId']['videoId'],
                    'publishedAt': item['snippet']['publishedAt']
                    },
                items
            )
        )
        next_page_token = response.get('nextPageToken')
        return (items,next_page_token)

    def _inflate_video_list(self,video_list):
        list = video_list
        while True:
            l = list[:50]
            list = list[50:]
            if not l:
                break
            self._inflate_small_video_list( l )
        return video_list

    def _inflate_small_video_list(self,video_list):
        ids = map( lambda video: video['videoId'], video_list )
        ids = '%2C'.join( ids )
        r = self._videos_request( part='statistics', id=ids )
        response = r.json()
        items = response['items']
        for item in items:
            id = item['id']
            videos = list( filter( lambda v: v['videoId'] == id, video_list ) )
            if len(videos) != 1:
                raise Exception('Videos count != 1')
            video = videos[0]
            #video['statistics'] = item['statistics']
            video.update( item['statistics'] )
        return video_list

    def _channels_request(self,**args):
        r = self._http_get( 'https://www.googleapis.com/youtube/v3/channels', args )
        return r

    def _playlistitems_request(self,**args):
        r = self._http_get( 'https://www.googleapis.com/youtube/v3/playlistItems', args )
        return r

    def _videos_request(self,**args):
        r = self._http_get( 'https://www.googleapis.com/youtube/v3/videos', args )
        return r

    def _http_get(self,url,args):
        api_key  = self._api_key
        args['key'] = api_key
        arguments = [ '%s=%s'%(k,v) for (k, v) in args.items() ]
        query_string = '&'.join( arguments )
        r = requests.get( url + '?' + query_string )
        return r