import argparse
import os
from os.path import join as pj
import sys
from shutil import copyfile
from youtubevideolist import YoutubeVideoList
import yaml

CONFIG_FILENAME = '.youtubevideolistrc'
CONFIG_EXAMPLE = '.youtubevideolistrc-dist'

class Command:

    def _create_config(self,args):
        sharepath = pj( sys.prefix, 'share' )
        source = pj( sharepath, CONFIG_EXAMPLE )
        homedir = os.environ['HOME']
        destination = pj( homedir, CONFIG_EXAMPLE )
        copyfile( source, destination )
        print( 'Config file example created : %s' % destination )

    def _list_videos(self,args):
        channel = args.channel
        homedir = os.environ['HOME']
        config_filepath = pj( homedir, CONFIG_FILENAME )
        with open( config_filepath, "r" ) as stream:
            config = yaml.load(stream)
        y = YoutubeVideoList( config['youtube_api_key'] )
        video_list = y.get_videolist( channel )
        if args.order == 'publishedAt':
            video_list = sorted( video_list, key=lambda v: v['publishedAt'] )
        elif args.order == 'viewCount':
            video_list = sorted( video_list, key=lambda v: int(v['viewCount']) )
        for video in video_list:
            comment = [ '#', video['title'] ]
            if args.field:
                comment.append( '(' + video[args.field] + ')' )
            comment.append( ':' )
            print( ' '.join(comment) )
            print( 'https://www.youtube.com/watch?v=%s' % video['videoId'] )

    def run(self,args=[]):
        parser = argparse.ArgumentParser()
        subparsers = parser.add_subparsers()
        config = subparsers.add_parser( 'config', help='Create configuration file example' )
        config.set_defaults(func=self._create_config)
        list = subparsers.add_parser( 'list', help='List YouTube videos.' )
        list.set_defaults(func=self._list_videos)
        list.add_argument( 'channel', help='YouTube channel' )
        list.add_argument( '--field', help='publishedAt,viewCount' )
        list.add_argument( '--order', help='publishedAt,viewCount' )
        args = parser.parse_args(args)
        if not hasattr(args,'func'):
            parser.print_help()
            parser.exit( status=1 )
        args.func( args )