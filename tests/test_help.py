import sys, os
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from youtubevideolist.command import Command
import pytest

def test_command():
    command = Command()
    with pytest.raises(SystemExit) as e:
        parser = command.run(['-h'])
    print( 'exit_code = %d' %  e.value.code )
    assert e.value.code == 0