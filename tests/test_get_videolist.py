import sys, os
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from youtubevideolist import YoutubeVideoList
import yaml

def test_get_videolist():
    homedir = os.environ['HOME']
    config_filepath = os.path.join( homedir, '.youtubevideolistrc' )
    stream = open( config_filepath, "r" )
    config = yaml.load(stream)
    y = YoutubeVideoList( config['youtube_api_key'] )
    #video_list = y.get_videolist('AbbaVEVO')
    video_list = y.get_videolist('joueurdugrenier')
    print()
    print( yaml.dump(video_list,default_flow_style=False) )
    print( len(video_list) )
    assert len(video_list) > 0