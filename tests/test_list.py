import sys, os
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from youtubevideolist.command import Command
import pytest

def test_list():
    command = Command()
    parser = command.run(['list','joueurdugrenier'])

def test_list_publishedAt():
    command = Command()
    parser = command.run(['list','joueurdugrenier','--field','publishedAt'])

def test_list_viewCount():
    command = Command()
    parser = command.run(['list','joueurdugrenier','--field','viewCount'])

def test_list_order():
    command = Command()
    parser = command.run(['list','joueurdugrenier','--field','publishedAt','--order','publishedAt'])