from setuptools import setup, find_packages

setup(
    name = "youtube-video-list",
    version = "0.1",
    packages = find_packages(),
    scripts = ['bin/youtubevideolist'],
    data_files = [
        ( 'share', ['share/.youtubevideolistrc-dist'] )
    ],
    install_requires = [
        'pyyaml',
        'requests'
    ]
)
